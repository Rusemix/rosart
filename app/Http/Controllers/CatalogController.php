<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalog;
use App\Category;
use App\CategoryParents;
use App\CategoryRelation;


class CatalogController extends Controller
{
    //
    public function index(Request $request){

        $sort = [
            'name' => 'created_at',
            'sort' => 'DESC',
        ];
        if(!empty($request['params']['sort'])){
            $sortName = $request['params']['sort'];

            switch ($sortName){
                case  'По убыванию цены':
                    $sort = [
                        'name' => 'price',
                        'sort' => 'DESC',
                    ];
                    break;
                case  'По возрастанию цены':
                    $sort = [
                        'name' => 'price',
                        'sort' => 'ASC',
                    ];
                    break;
                case  'Название А-Я':
                    $sort = [
                        'name' => 'name',
                        'sort' => 'ASC',
                    ];
                    break;
                case  'Название Я-А':
                    $sort = [
                        'name' => 'name',
                        'sort' => 'DESC',
                    ];
                    break;
            }
        }

        $skip = 0;
        if(!empty($request['params']['pageId'])){
            $skip = ($request['params']['pageId']-1)*12;
        }

        $priceFrom = '1';
        if(!empty($request['params']['priceFrom'])){
            $priceFrom = $request['params']['priceFrom'];
        }

        $priceTo = 70000;
        if(!empty($request['params']['priceTo'])){
            $priceTo = $request['params']['priceTo'];
        }

        if(!empty($request['params']['idFilters'])){
            $new_array = array_filter($request['params']['idFilters'], function($element) {
                return !empty($element);
            });
            if($new_array){
                $idCategory =  CategoryRelation::whereIn('category_id', $new_array)->get();
                foreach($idCategory as $value){
                    $idCatalog[] = $value['catalog_id'];
                }
                $countItems = Catalog::where('price', '>', $priceFrom)->where('price', '<', $priceTo)->whereIn('id', $idCatalog)->get();
                $out['products'] = Catalog::where('price', '>', $priceFrom)->where('price', '<', $priceTo)->whereIn('id', $idCatalog)->take(12)->skip($skip)->orderByRaw($sort['name'].' '.$sort['sort'])->get();
            }else{
                $countItems = Catalog::where('price', '>', $priceFrom)->where('price', '<', $priceTo)->get();
                $out['products'] = Catalog::where('price', '>', $priceFrom)->where('price', '<', $priceTo)->take(12)->skip($skip)->orderByRaw($sort['name'].' '.$sort['sort'])->get();
            }
        }else{
            $countItems = Catalog::where('price', '>', $priceFrom)->where('price', '<', $priceTo)->get();
            $out['products'] = Catalog::where('price', '>', $priceFrom)->where('price', '<', $priceTo)->take(12)->skip($skip)->orderByRaw($sort['name'].' '.$sort['sort'])->get();
        }


        $model = new CategoryParents();
        foreach($model::all() as $value){
            $out['filters'][] = [
                'name' => $value,
                'data' => $model->category($value['id'])
            ];
        }

        $out['pageAll'] = ceil(count($countItems)/12);


        return $out;

    }

}
