<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class CategoryParents extends Model
{

    public function category($id)
    {
        $model = Category::where('parent_id', $id)->get();
        return $model;
    }

}
