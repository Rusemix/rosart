<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    public function relation()
    {
        return $this->hasMany('App\CategoryRelation');
    }
}
