<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogs', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->integer('price');
            $table->string('image');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });

        DB::table('catalogs')->insert([
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '35000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '20000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '55000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '44000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '32000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '35000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '22000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '65000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '35000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '32000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '15000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '16000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '17000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '33000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '30000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '37000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '39000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '40000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '35000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '30000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '51000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '52000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '33000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '39000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '34000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '31000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '28000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '29000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '27000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '28000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '21000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '59000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '57000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '54000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '53000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '52000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '51000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Цикл Литографий  «Маскарад» Лит.№26',
                'price' => '50000',
                'image' => '/img/pic1.jpg',
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogs');
    }
}
