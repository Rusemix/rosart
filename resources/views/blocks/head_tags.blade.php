<title>Title</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noindex, nofollow">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />



<link rel="stylesheet" href="/css/slick.css">
<link rel="stylesheet" href="/css/jquery-ui.min.css">
<link rel="stylesheet" href="/css/animate.css">
<link rel="stylesheet" href="/css/justifiedgallery.min.css">
<link rel="stylesheet" href="/css/colorbox.css">
<link rel="stylesheet" href="/css/main.css">

<!-- <link href="favicon.ico" rel="shortcut icon" type="image/x-icon"> -->


<!--[if IE]>
  <script src="/js/html5shiv.min.js"></script>
  <script src="/js/jquery.placeholder.min.js"></script>
  <script>$(document).ready(function(){$('input, textarea').placeholder()})</script>
<![endif]-->