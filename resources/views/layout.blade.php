<!doctype html>
<html lang="ru">
<head>
    @include('blocks.head_tags')
</head>
<body class="topPadidng">
@include('blocks.header')
<div class="adb_pic_wd" style="background-image: url(img/category_bg.jpg);">
    <div class="wrapper">
        <ul class="breadcrumbs">
            <li><a class="underscore" href="index.php">Главная</a></li>
            <li><a class="underscore" href="index.php">Каталог</a></li>
            <li><span>Живопись</span></li>
        </ul>
    </div>
</div>

@yield('content')

@include('blocks.footer')
@include('blocks.dialog')


<script src="/js/app.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script> window.jQuery || document.write('<script src="/js/jquery-1.11.2.min.js"><\/script>')</script>
<script src="/js/migrate.js"></script>
<script src="/js/svg4everybody.min.js"></script>
<script src="/js/slick.min.js"></script>
<script src="/js/jquery.formstyler.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/jquery.justifiedgallery.min.js"></script>
<script src="/js/jquery.colorbox-min.js"></script>
<script src="/js/main.js"></script>
<script>
    new WOW().init();
</script>

<script>
    $(document).ready(function () {
        $('#active_menu_3').addClass('active');
    });
</script>

</body>
</html>